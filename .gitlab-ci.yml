stages:
  - build
  - scan
  - deploy

variables:
  LOG_LEVEL: "info"

include:
  - project: 'utility/project-templates/ci-templates'
    file: '/docker.yml'
  - project: 'ori-rad/ci-pipeline-utilities/deployment'
    file: '/deployment.yml'

.define_component: &define_component |
  export THE_DOCKERFILE="Dockerfile"
  export THE_IMAGE="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  export BUILD_ARGS="--build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg CI_PROJECT_URL=${CI_PROJECT_URL}"

.build_component:
  stage: build
  image: 
    name: gitlab.dhe.duke.edu:4567/utility/images/kaniko-executor:debug-v0.19.0
    entrypoint: [""]
  before_script:
    - *define_component
  extends: .kaniko_build
  only:
    - master

.scan_component:
  stage: scan
  before_script:
    - *define_component
  extends: .docker_scan
  only:
    - master

.deploy_to_cluster:
  stage: deploy
  extends: .deploy
  script:
    - echo "Deploy app named ${CI_ENVIRONMENT_NAME} using url ${CI_ENVIRONMENT_URL}"
    - source /usr/local/bin/deploy
    - CONTAINER_ENV="SECRET_KEY_BASE"
    - export HELM_CHART_NAME=${HELM_CHART_NAME:-${CI_PROJECT_NAME}}
    - check_required_environment "CI_PROJECT_NAME CI_PROJECT_DIR CI_COMMIT_REF_SLUG CI_REGISTRY CI_REGISTRY_IMAGE CI_ENVIRONMENT_NAME CI_ENVIRONMENT_URL CI_ENVIRONMENT_SLUG CI_COMMIT_SHORT_SHA HELM_TOKEN HELM_USER PROJECT_NAMESPACE CLUSTER_SERVER CI_JOB_ID ${CONTAINER_ENV}"
    - export DEPLOYMENT_NAME=$([ ${#CI_ENVIRONMENT_NAME} -lt $((52 - ${#HELM_CHART_NAME})) ] && echo -n ${CI_ENVIRONMENT_NAME}-${HELM_CHART_NAME} || echo -n $(expr substr $CI_ENVIRONMENT_NAME 1 $((45 - ${#HELM_CHART_NAME})))-$(echo -n $CI_ENVIRONMENT_NAME | sha256sum | cut -d' ' -f1 | head -c16 | perl -le 'print hex(<>);' | base32 | head -c6 | tr '[:upper:]' '[:lower:]')-${HELM_CHART_NAME})
    - echo "Generated deployment name is ${DEPLOYMENT_NAME}"
    - export WATCH_DEPLOYMENT="${DEPLOYMENT_NAME}"
    - export DB__USER=postgres
    - export DB__PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    - export DB__NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
    - export DB__SERVER="${DEPLOYMENT_NAME}-db"
    - export HELM_CHART_DIR="${CI_PROJECT_DIR}/helm-chart/${HELM_CHART_NAME}"
    - cluster_login
    - helm3 lint ${HELM_CHART_DIR} 
      --set image.repository="${CI_REGISTRY_IMAGE}"
      --set-string image.tag="${CI_COMMIT_SHORT_SHA}"
      --set url="${CI_ENVIRONMENT_URL}"
      --set environment="${CI_ENVIRONMENT_SLUG}"
      --set-string git_commit="${CI_COMMIT_SHORT_SHA}"
      --set git_ref="${CI_COMMIT_REF_SLUG}"
      --set ci_job_id="${CI_JOB_ID}"
      --set-string registry.root="${CI_REGISTRY}"
      --set-string "${DATABASE_URL:+database_url=${DATABASE_URL},}db.image=${DB_IMAGE},db.name=${DB__NAME},db.user=${DB__USER},db.password=${DB__PASSWORD},db.server=${DB__SERVER}${PERSIST_DB:+,db.persistent=1}"
      --set-string containerEnv.RAILS_SERVE_STATIC_FILES="yes"
      --set-string "$(env | grep -E "^(${CONTAINER_ENV// /|})=" | sed 's/^/containerEnv./; s/,/\\,/g' | paste -sd',')"
    - helm3 upgrade "${DEPLOYMENT_NAME}" ${HELM_CHART_DIR} 
      --namespace ${PROJECT_NAMESPACE}
      --atomic --reset-values --debug --wait --install
      --set image.repository="${CI_REGISTRY_IMAGE}"
      --set-string image.tag="${CI_COMMIT_SHORT_SHA}"
      --set url="${CI_ENVIRONMENT_URL}"
      --set environment="${CI_ENVIRONMENT_SLUG}"
      --set-string git_commit="${CI_COMMIT_SHORT_SHA}"
      --set git_ref="${CI_COMMIT_REF_SLUG}"
      --set ci_job_id="${CI_JOB_ID}"
      --set-string registry.root="${CI_REGISTRY}"
      --set-string "${DATABASE_URL:+database_url=${DATABASE_URL},}db.image=${DB_IMAGE},db.name=${DB__NAME},db.user=${DB__USER},db.password=${DB__PASSWORD},db.server=${DB__SERVER}${PERSIST_DB:+,db.persistent=1}"
      --set-string containerEnv.RAILS_SERVE_STATIC_FILES="yes"
      --set-string "$(env | grep -E "^(${CONTAINER_ENV// /|})=" | sed 's/^/containerEnv./; s/,/\\,/g' | paste -sd',')"
    - watch_deployment

.decommission_from_cluster:
  stage: deploy
  extends: .decommission
  script:
    - source /usr/local/bin/decommission
    - check_required_environment "HELM_CHART_NAME CI_ENVIRONMENT_NAME HELM_TOKEN HELM_USER PROJECT_NAMESPACE CLUSTER_SERVER"
    - export DEPLOYMENT_NAME=$([ ${#CI_ENVIRONMENT_NAME} -lt $((52 - ${#HELM_CHART_NAME})) ] && echo -n ${CI_ENVIRONMENT_NAME}-${HELM_CHART_NAME} || echo -n $(expr substr $CI_ENVIRONMENT_NAME 1 $((45 - ${#HELM_CHART_NAME})))-$(echo -n $CI_ENVIRONMENT_NAME | sha256sum | cut -d' ' -f1 | head -c16 | perl -le 'print hex(<>);' | base32 | head -c6 | tr '[:upper:]' '[:lower:]')-${HELM_CHART_NAME})
    - cluster_login
    - init_helm_with_tiller
    - helm delete --purge "${DEPLOYMENT_NAME}"
    - decommission_tiller
  environment:
    action: stop
  when: manual

build:
  variables:
    BUILD_CACHE: "true"
  extends: .build_component

scan:
  extends: .scan_component

deploy:
  extends: .deploy_to_cluster
  environment:
    name: development
    url: https://${CI_ENVIRONMENT_SLUG}-${CI_PROJECT_NAME}.cats-a.dhe.duke.edu
    on_stop: decommission
  only:
    - master

decommission:
  extends: .decommission_from_cluster
  environment:
    name: development
  when: manual
  only:
    - master
