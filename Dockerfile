FROM gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/ruby/ruby:2-6-5

ENV APP_HOME=/opt/app-root/src

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL
LABEL git_repository_url=${CI_PROJECT_URL}

# https://github.com/rails/rails/issues/32947
# we can pass actual value from CI ENV as build-arg if needed
ARG SECRET_KEY_BASE=1

# yarn/npm
RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo \
  && curl -sL https://rpm.nodesource.com/setup_12.x | bash - \
  && dnf install -y yarn postgresql libpq-devel findutils \
  && dnf clean all \
  && yarn add stimulus

WORKDIR ${APP_HOME}
# Allow sassc gem to work cross-platform:
# https://github.com/sass/sassc-ruby/issues/146
ENV BUNDLE_BUILD__SASSC=--disable-march-tune-native

COPY Gemfile* ${APP_HOME}/
RUN bundle install --retry 3 \
      && rm -rf `gem env gemdir`/cache/*.gem \
      && find `gem env gemdir`/gems/ -name "*.c" -delete \
      && find `gem env gemdir`/gems/ -name "*.o" -delete

# Copy the application into the container
COPY . $APP_HOME

RUN yarn install --check-files \
      && RAILS_ENV=production rails assets:precompile \
      && rm -rf tmp/cache vendor/assets

RUN chmod -R g=rwX ${APP_HOME}

# Start the main process.
CMD ["puma"]
