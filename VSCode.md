VSCode Remote Container Development
---

This application is configured to allow development 
inside the application container, using 
[VSCode Remote Container Development](https://code.visualstudio.com/docs/remote/containers)


#### Getting Started

VSCode Remote Container Development requires software to be installed on the
host machine:

- [Install docker](https://docs.docker.com/get-docker/) for the operating system of your host.
- [Install docker-compose](https://docs.docker.com/compose/install/) if it is not included with the
docker installation itself (Docker for Mac and Docker for Windows both install docker-compose).
- [Install VSCode](https://code.visualstudio.com/docs/setup/setup-overview)
- [Install VSCode Remote Container Extension](https://code.visualstudio.com/docs/remote/containers#_installation)

##### Initial Setup

Before launching VSCode for this project for the first 
time, or if the extended server image is ever deleted
from your local docker registry, you must build the
application image.

This is because we reference the image in a second container
designed to run webpacker in our `docker-compose.yml` file, and
if the image does not exist, the Remote Container build will 
fail. Run the following command from the commandline in the 
root of the project to build the image:
```
docker-compose build server
```

If you do not have a directory called `workspace` in your host computer
home directory, create an empty directory before launching VSCode.
```
mkdir ~/workspace
```

#### Usage

When you launch VSCode for this project, it will always
initially launch an editor on the host file system. Then 
it will prompt you with 
```
Folder contains a dev container configuration file. 
Reopen folder to develop in a container ([learn more](https://aka.ms/vscode-remote/docker))
```
and buttons to **Dont Show Again**, **Reopen in Container**, 
and an **X**. For most development, choose **Reopen in 
Container**. If you really just want to change a file, 
such as this file, without launching the application in 
the container, hit the **X** button, but never choose **Dont 
Show Again**, unless you always want it to open in the 
container.

When you choose **Reopen in Container** it will do the 
following:
- use the `docker-compose.yml` that we have configured for 
the project, along with a VSCode extension 
docker-compose yaml file (see VSCode Configuration) with 
the Dockerfile
- run a new container
- add VSCode libraries to that container (first time 
only)
- add ruby extensions that we have configured (first 
time only, see VSCode Configuration) to the container\
- present you with a terminal inside VSCode

VScode uses docker-compose to launch the **server**,
**webpacker**, and **db** services defined in our
`docker-compose.yml` file.

When you exit VSCode, or close the window for this 
project, VSCode will run docker-compose stop, and the 
VSCode extended  **server**, **db**, and 
**webpacker** containers will stop (you can verify this after
a few seconds using docker-compose ps in the root of the
project on the host).

When you launch VSCode again, it will not have to build 
the extended **server** container again, so it will launch your 
initial terminal faster. 

If you should happen to remove the
extended **server** container, such as with docker-compose 
down, or docker rm, then the next time you launch VSCode 
on the project, it will rebuild the extended container.

#### Working in the Container

All of the application source in the repository is 
volume mounted into the container at the path `/workspace`.

Volume mounting is a mechanism that docker supports to allow
directories on the host to be synchronized into the running container.
The path on the host does not have to match the path in the container, e.g
a directory `~/projects/foo` on the host can be mapped to any path, such as `/bar`,
in the container.

Changes in files/directories in volume mounted directories, **including** creation and
deletion, both in the container and on the host, are synchronized immediately.

This means:
- changes you make to the code in VSCode are saved onto the host, while also getting immediately
displayed in the application using standard hot reload functionality. 
- commands run in the container, like rails g, rails d, rails db:migrate, etc., will create, delete, and
modify files and directories within the application source on the host file system.

You can open many terminals in the VSCode IDE, e.g;
1. run puma
2. run rspec
3. run rails g
4. tail the development log

All of these will run inside the application container. That 
means that these run with the OS, libraries, Ruby, and gems,
built into the container, just as they would run in the production
deployed application using the `Dockerfile` we have created.

The VSCode Ruby extensions (see VSCode Configuration) also run inside the container, 
using the container OS, libraries, ruby and gems. This means that code linters, navigation
aids, integrated debuggers, etc. work as if the application is running natively on the
host.

This makes developing in a containerized application feel more like developing on 
the host machine itself, while ensuring parity between the local development
and production environment.

#### Changing Gems and Packages

Any time you change the `Dockerfile`, `Gemfile`, or use yarn 
to install a new package, you should rebuild the VSCode 
container:
- hit the green button with '>< Dev Container:...' on 
the bottom right of the VSCode window, or type 
CMD-Shift-P to bring up the command template
- choose Remote-Container: Rebuild Container

This will run the same initial docker-compose command to 
rebuild the extended container, and launch inside the 
new one with a terminal.

#### Local Gem Development

The `.devcontainer/docker-compose.yml` automatically volume mounts a directory in
the developers host home directory, `~/workspace`, into a container directory
`/workspace_gems`. This allows you to work with gem/package directories contained
in that directory on your host filesystem inside your containerized application,
with hot reloading capabailities supported by rails/webpacker.

For example, refer to local gem folders in your Gemfile using the following:
```
# Duke Gems - Development
gem 'duke_rad_assets',              :path => "/workspace_gems/duke_rad_assets/"
gem 'duke_rad_materialize',         :path => "/workspace_gems/duke_rad_materialize/"
```

#### VSCode Configuration

The VSCode Remote Container configuration files are stored in the .devcontainer
directory. This consists of:
- `.devcontainer.json`: [this](https://code.visualstudio.com/docs/remote/containers#_devcontainerjson-reference) instructs 
VSCode how to manage remote development. We use docker-compose. 
We also ensure that the following ruby VSCode extensions
(see documentation for each online) are always added to 
the container when it is built:
  - rebornix.ruby
  - hoovercj.ruby-linter
  - vortizhe.simple-ruby-erb
  - jemmyw.rails-fast-nav
  - tmikoss.rails-latest-migration
  - kaiwood.endwis
- `docker-compose.yml`: this extends the project 
docker-compose.yml with VSCode specific features. One
thing is that it does not actually start up the
application as a server. Instead, it just runs a
long-running shell loop that keeps the container alive.
This allows to launch additional terminals inside
the container as needed.

We should modify this `docker-compose.yml` to do things, such
as volume mounting specific directories, that are specific to
VSCode development that we do not want to affect developers 
that do not use VSCode for local development.
